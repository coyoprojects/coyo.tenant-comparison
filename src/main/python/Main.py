#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Main"""

import os
import time
import logging
from pathlib import Path

from lib.AppConfig import AppConfig
from lib.coyo.CoyoData import CoyoData
from gui.MainGUI import MainGUI

# COYO settings
CLOUD_BASE_URL = 'https://coyocloud.com'
CLOUD_MANAGE_USERNAME = ''
CLOUD_MANAGE_PASSWORD = ''
CC_BASE_URL = 'https://my.coyoapp.com'
CC_API_USERNAME = ''
CC_API_PASSWORD = ''

# Logging configuration
logging_loglevel = logging.DEBUG
logging_datefmt = '%d-%m-%Y %H:%M:%S'
logging_format = '[%(asctime)s] [%(levelname)-5s] [%(module)-20s:%(lineno)-4s] %(message)s'
logging_logfile = str(Path.home()) + '/logs/coyo-user-exporter.application-' + time.strftime('%d-%m-%Y-%H-%M-%S') + '.log'

def _initialize_logger():
    """Initializes the logging utility"""
    basedir = os.path.dirname(logging_logfile)

    if not os.path.exists(basedir):
        os.makedirs(basedir)

    logging.basicConfig(level=logging_loglevel,
                        format=logging_format,
                        datefmt=logging_datefmt)

    handler_file = logging.FileHandler(logging_logfile, mode='w', encoding=None, delay=False)
    handler_file.setLevel(logging_loglevel)
    handler_file.setFormatter(logging.Formatter(fmt=logging_format, datefmt=logging_datefmt))
    logging.getLogger().addHandler(handler_file)

if __name__ == '__main__':
    _initialize_logger()

    logging.info('##############################')
    logging.info('#   COYO Tenant Comparison   #')
    logging.info('##############################\n')

    coyodata = CoyoData(
        cloud_baseurl=CLOUD_BASE_URL,
        cloud_manage_username=CLOUD_MANAGE_USERNAME,
        cloud_manage_password=CLOUD_MANAGE_PASSWORD,
        cc_baseurl=CC_BASE_URL,
        cc_api_username=CC_API_USERNAME,
        cc_api_password=CC_API_PASSWORD)

    appconfig = AppConfig()
    gui = MainGUI(appconfig, coyodata)
