#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Main widget"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QThreadPool
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QMessageBox, QDesktopWidget, QGridLayout, QLabel, QLineEdit, QPushButton, QFileDialog, QCheckBox, QComboBox

from lib.Data import Data
from lib.coyo.CoyoOps import CoyoOps
from lib.coyo.CoyoData import CoyoData
from gui.components.StatisticsDialog import StatisticsDialog
from gui.threads.DownloadCloudTenantsThread import DownloadCloudTenantsThread
from gui.threads.DownloadCCTenantsThread import DownloadCCTenantsThread
from gui.threads.DownloadTenantsThread import DownloadTenantsThread
from gui.threads.ExportJSONThread import ExportJSONThread

class MainWidget(QWidget):
    """Main widget GUI"""

    def __init__(self, appconfig, log, coyodata):
        """Initializes the main widget

        :param appconfig: The AppConfig
        :param log: The (end user) message log
        :param coyodata: The CoyoData
        """
        super().__init__()

        logging.debug('Initializing MainWidget')

        self.appconfig = appconfig
        self.log = log
        self.components = []
        self.coyodata = coyodata
        
        self.font_label_header = QFont()
        self.font_label_header.setBold(True)

        self.threadpool = QThreadPool()
        logging.debug('Multithreading with maximum {} threads.'.format(self.threadpool.maxThreadCount()))

        self.loaded_whitelist = False
        self.data = Data()

    def init_ui(self):
        """Initiates application UI"""
        logging.debug('Initializing MainWidget GUI')

        self.grid = QGridLayout()
        self.grid.setSpacing(10)

        self.label_empty = QLabel('')
        self.label_details_cloud_server = QLabel('COYO Cloud - Server details')
        self.label_details_cc_server = QLabel('COYO Customer Center - Server details')
        self.label_details_whitelist = QLabel('Whitelist')
        self.label_details_actions = QLabel('Actions')
        self.label_cloud_baseurl = QLabel('Cloud base URL')
        self.label_cloud_manage_username = QLabel('Management username')
        self.label_cloud_manage_password = QLabel('Management password')
        self.label_cc_baseurl = QLabel('CC base URL')
        self.label_cc_api_username = QLabel('API username')
        self.label_cc_api_password = QLabel('API password')

        self.label_details_cloud_server.setFont(self.font_label_header)
        self.label_details_cc_server.setFont(self.font_label_header)
        self.label_details_whitelist.setFont(self.font_label_header)
        self.label_details_actions.setFont(self.font_label_header)

        self.edit_cloud_baseurl = QLineEdit()
        self.edit_cloud_baseurl.setText(self.coyodata.cloud_baseurl)
        self.edit_cloud_manage_username = QLineEdit()
        self.edit_cloud_manage_username.setText(self.coyodata.cloud_manage_username)
        self.edit_cloud_manage_password = QLineEdit()
        self.edit_cloud_manage_password.setText(self.coyodata.cloud_manage_password)
        self.edit_cc_baseurl = QLineEdit()
        self.edit_cc_baseurl.setText(self.coyodata.cc_baseurl)
        self.edit_cc_api_username = QLineEdit()
        self.edit_cc_api_username.setText(self.coyodata.cc_api_username)
        self.edit_cc_api_password = QLineEdit()
        self.edit_cc_api_password.setText(self.coyodata.cc_api_password)

        self.button_cloud_start = QPushButton('Start download')
        self.button_cloud_start.clicked[bool].connect(self._start_preparing_cloud_processing)
        self.button_cloud_save_json = QPushButton('Save as JSON')
        self.button_cloud_save_json.clicked[bool].connect(self._save_cloud_json)
        self.button_cc_start = QPushButton('Start download')
        self.button_cc_start.clicked[bool].connect(self._start_preparing_cc_processing)
        self.button_cc_save_json = QPushButton('Save as JSON')
        self.button_cc_save_json.clicked[bool].connect(self._save_cc_json)
        self.button_load_whitelist = QPushButton('Load (JSON array)')
        self.button_load_whitelist.clicked[bool].connect(self._load_whitelist)
        self.button_reset_whitelist = QPushButton('Reset')
        self.button_reset_whitelist.clicked[bool].connect(self._reset_whitelist)
        self.button_compare = QPushButton('Start comparison')
        self.button_compare.clicked[bool].connect(self._start_preparing_processing)
        self.button_compare_clean = QPushButton('Clear and start comparison')
        self.button_compare_clean.clicked[bool].connect(self._start_preparing_processing_clean)
        self.button_show_statistics = QPushButton('Show statistics')
        self.button_show_statistics.clicked[bool].connect(self._show_statistics)
        self.button_save_json = QPushButton('Save as JSON')
        self.button_save_json.clicked[bool].connect(self._save_json)

        self.components.append(self.edit_cloud_baseurl)
        self.components.append(self.edit_cloud_manage_username)
        self.components.append(self.edit_cloud_manage_password)
        self.components.append(self.edit_cc_baseurl)
        self.components.append(self.edit_cc_api_username)
        self.components.append(self.edit_cc_api_password)
        self.components.append(self.button_cloud_start)
        self.components.append(self.button_cloud_save_json)
        self.components.append(self.button_cc_start)
        self.components.append(self.button_cc_save_json)
        self.components.append(self.button_load_whitelist)
        self.components.append(self.button_reset_whitelist)
        self.components.append(self.button_compare)
        self.components.append(self.button_compare_clean)
        self.components.append(self.button_show_statistics)
        self.components.append(self.button_save_json)

        curr_gridid = 1

        self.grid.addWidget(self.label_details_cloud_server, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_cloud_baseurl, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_cloud_baseurl, curr_gridid, 1, 1, 2)

        curr_gridid += 1
        self.grid.addWidget(self.label_cloud_manage_username, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_cloud_manage_username, curr_gridid, 1, 1, 2)

        curr_gridid += 1
        self.grid.addWidget(self.label_cloud_manage_password, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_cloud_manage_password, curr_gridid, 1, 1, 2)

        curr_gridid += 1
        self.grid.addWidget(self.button_cloud_start, curr_gridid, 1, 1, 1)
        self.grid.addWidget(self.button_cloud_save_json, curr_gridid, 2, 1, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_cc_server, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_cc_baseurl, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_cc_baseurl, curr_gridid, 1, 1, 2)

        curr_gridid += 1
        self.grid.addWidget(self.label_cc_api_username, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_cc_api_username, curr_gridid, 1, 1, 2)

        curr_gridid += 1
        self.grid.addWidget(self.label_cc_api_password, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_cc_api_password, curr_gridid, 1, 1, 2)

        curr_gridid += 1
        self.grid.addWidget(self.button_cc_start, curr_gridid, 1, 1, 1)
        self.grid.addWidget(self.button_cc_save_json, curr_gridid, 2, 1, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_whitelist, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_load_whitelist, curr_gridid, 0, 1, 2)
        self.grid.addWidget(self.button_reset_whitelist, curr_gridid, 2, 1, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_actions, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_compare, curr_gridid, 0, 1, 2)
        self.grid.addWidget(self.button_compare_clean, curr_gridid, 2, 1, 1)

        curr_gridid += 1
        self.grid.addWidget(self.button_show_statistics, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_save_json, curr_gridid, 0, 1, 3)

        self.setLayout(self.grid)
        self._reset_enabled()

    def _reset_enabled(self):
        """Resets all component to initial state"""
        logging.debug('Resetting components to enabled state')

        self.data = Data()
        self._enable()

    def _disable(self):
        """Resets all component to disabled state"""
        logging.debug('Disabling components')

        for comp in self.components:
            comp.setEnabled(False)

    def _enable(self):
        """Resets all component to enabled state"""
        logging.debug('Enabling components')

        for comp in self.components:
            comp.setEnabled(True)
        if not self.data.cloudtenants:
            self.button_cloud_save_json.setEnabled(False)
        if not self.data.cctenants:
            self.button_cc_save_json.setEnabled(False)
        if not self.data.difftenants:
            self.button_save_json.setEnabled(False)

    def _show_dialog_critical(self, window_title, text, inf_text, detail_text):
        """Shows a dialog, critical

        :param window_title: The window title
        :param text: The text
        :param inf_text: The information text
        :param detail_text: The detail text
        """
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setStandardButtons(QMessageBox.Ok)

        msg.setWindowTitle(window_title)
        msg.setText(text)
        msg.setInformativeText(inf_text)
        msg.setDetailedText(detail_text)
        
        return msg.exec_()

    def _show_statistics(self):
        """Shows a statistics window"""
        statistics = StatisticsDialog(self.data)
        statistics.init_ui()
        statistics.exec_()

    def _match_tenants(self):
        """Matches tenants. Ignores tenants from the whitelist."""
        logging.info('Matching tenants...')
        self.data.difftenants = []
        self.data.difftenants_whitelisted = []
        for c4tenant in self.data.cloudtenants:
            found = False
            for c4cctenant in self.data.cctenants:
                if c4tenant['id'] == c4cctenant['coyoTenantId']:
                    found = True
                    break
            if not found:
                add = True
                if 'authorities' in c4tenant:
                    for authority in c4tenant['authorities']:
                        if authority in self.data.whitelist:
                            add = False
                            break
                if add:
                    self.data.difftenants.append(c4tenant)
                else:
                    self.data.difftenants_whitelisted.append(c4tenant)

    def _callback_cloud_processing_result(self, tenants):
        """The processing callback, on result

        :param tenants: The tenant list
        """
        logging.debug('Callback: Cloud processing result')

        self.log('Downloaded {} cloud tenants.'.format(len(tenants)))
        self.data.cloudtenants = tenants

    def _callback_cloud_processing_error(self, ex):
        """The processing callback, on error
        
        :param ex: The exception
        """
        logging.debug('Callback: Cloud processing error')

        logging.error('Failed to download cloud tenants: "{}"'.format(ex))
        self.log('Failed to download cloud tenants.')
        self._reset_enabled()
        self._show_dialog_critical('Error', 'Download failed', 'Could not download all cloud tenants: "{}"'
            .format(ex), None)

    def _callback_cloud_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')
        self._enable()

    def _callback_cc_processing_result(self, tenants):
        """The processing callback, on result

        :param tenants: The tenant list
        """
        logging.debug('Callback: Customer Center processing result')

        self.log('Downloaded {} Customer Center tenants.'.format(len(tenants)))
        self.data.cctenants = tenants

    def _callback_cc_processing_error(self, ex):
        """The processing callback, on error
        
        :param ex: The exception
        """
        logging.debug('Callback: Customer Center processing error')

        logging.error('Failed to download Customer Center tenants: "{}"'.format(ex))
        self.log('Failed to download Customer Center tenants.')
        self._reset_enabled()
        self._show_dialog_critical('Error', 'Download failed', 'Could not download all Customer Center tenants: "{}"'
            .format(ex), None)

    def _callback_cc_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')
        self._enable()

    def _callback_processing_result(self, cloudtenants, cctenants):
        """The processing callback, on result

        :param cloudtenants: The cloud tenants
        :param cctenants: The Customer Center tenants
        """
        logging.debug('Callback: Processing result')

        self.log('Downloaded {} cloud and {} Customer Center tenants.'.format(len(cloudtenants), len(cctenants)))
        self.data.cloudtenants = cloudtenants
        self.data.cctenants = cctenants

    def _callback_processing_error(self, ex):
        """The processing callback, on error
        
        :param ex: The exception
        """
        logging.debug('Callback: Processing error')

        logging.error('Failed to download all tenants: "{}"'.format(ex))
        self.log('Failed to download all tenants.')
        self._reset_enabled()
        self._show_dialog_critical('Error', 'Download failed', 'Could not download all tenants: "{}"'.format(ex), None)

    def _callback_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')
        self._match_tenants()
        self._enable()

    def _load_whitelist(self):
        """Loads a whitelist"""
        logging.debug('Loading whitelist')

        try:
            self.loaded_whitelist = False
            self.data.whitelist = []
            self._disable()
            self.data.difftenants = []
            options = QFileDialog.Options()
            # options |= QFileDialog.DontUseNativeDialog
            filename, _ = QFileDialog.getOpenFileName(self,
                                                      'Open whitelist JSON file', '', 'JSON Files (*.json);;All Files (*)',
                                                      options=options)
            if filename:
                logging.debug('Importing whitelist from file "{}"'.format(filename))
                with open(filename, 'r', encoding='utf-8') as file_loaded:
                    self.data.whitelist = json.load(file_loaded)
                    self.loaded_whitelist = True
                    logging.info('Imported {} whitelisted tenants'.format(len(self.data.whitelist)))
                    self.log('Imported {} whitelisted tenants'.format(len(self.data.whitelist)))
            else:
                logging.debug('No file name given. Aborting...')
            self._enable()
        except Exception:
            logging.exception('Could not write JSON to file')
            self.log('Could not write JSON to file.')
            self._enable()

    def _reset_whitelist(self):
        """Resets a whitelist"""
        self.loaded_whitelist = False
        self.data.whitelist = []
        self.log('Whitelist reset. No whitelisted tenants listed.')
        self._enable()

    def _start_cloud_processing(self):
        """Starts the processing"""
        logging.debug('Starting cloud processing')
        logging.info('Downloading cloud tenants...')
        self.log('Downloading cloud tenants...')

        try:
            ops = CoyoOps(self.coyodata)
            thread = DownloadCloudTenantsThread(ops)
            thread.signals.result.connect(self._callback_cloud_processing_result)
            thread.signals.error.connect(self._callback_cloud_processing_error)
            thread.signals.finished.connect(self._callback_cloud_processing_finished)
            self.threadpool.start(thread)
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self._show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), None)
            self._reset_enabled()

    def _start_cc_processing(self):
        """Starts the processing"""
        logging.debug('Starting Customer Center processing')
        logging.info('Downloading Customer Center tenants...')
        self.log('Downloading Customer Center tenants...')

        try:
            ops = CoyoOps(self.coyodata)
            thread = DownloadCCTenantsThread(ops)
            thread.signals.result.connect(self._callback_cc_processing_result)
            thread.signals.error.connect(self._callback_cc_processing_error)
            thread.signals.finished.connect(self._callback_cc_processing_finished)
            self.threadpool.start(thread)
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self._show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), None)
            self._reset_enabled()

    def _start_processing(self):
        """Starts the processing"""
        logging.debug('Starting processing')
        logging.info('Downloading all tenants...')
        self.log('Downloading all tenants...')

        try:
            ops = CoyoOps(self.coyodata)
            thread = DownloadTenantsThread(ops)
            thread.signals.result.connect(self._callback_processing_result)
            thread.signals.error.connect(self._callback_processing_error)
            thread.signals.finished.connect(self._callback_processing_finished)
            self.threadpool.start(thread)
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self._show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), None)
            self._reset_enabled()

    def _start_preparing_cloud_processing(self):
        """Starts the processing process"""
        logging.debug('Starting preparing cloud processing')

        try:
            self._disable()
            errors = []
            baseurl = self.edit_cloud_baseurl.text()
            if not baseurl:
                logging.error('Invalid cloud base URL')
                errors.append('Please enter a cloud base URL')
            username = self.edit_cloud_manage_username.text()
            if not username:
                logging.error('Invalid cloud manage username')
                errors.append('Please enter a cloud manage username')
            password = self.edit_cloud_manage_password.text()
            if not password:
                logging.error('Invalid cloud manage password')
                errors.append('Please enter a cloud manage password')
            if errors:
                err_str = ''
                for i, err in enumerate(errors):
                    if i > 0:
                        err_str += '\n'
                    err_str += '- ' + err
                self._show_dialog_critical('Error', 'Parameters missing', 'Some basic parameters are missing.', err_str)
                self._reset_enabled()
            else:
                self.coyodata = CoyoData(cloud_baseurl=baseurl,
                                         cloud_manage_username=username,
                                         cloud_manage_password=password)
                self._start_cloud_processing()
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self._show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), err_str)
            self._reset_enabled()

    def _start_preparing_cc_processing(self):
        """Starts the processing process"""
        logging.debug('Starting preparing Customer Center processing')

        try:
            self._disable()
            errors = []
            baseurl = self.edit_cc_baseurl.text()
            if not baseurl:
                logging.error('Invalid Customer Center base URL')
                errors.append('Please enter a Customer Center base URL')
            username = self.edit_cc_api_username.text()
            if not username:
                logging.error('Invalid Customer Center API username')
                errors.append('Please enter a Customer Center API username')
            password = self.edit_cc_api_password.text()
            if not password:
                logging.error('Invalid Customer Center API password')
                errors.append('Please enter a Customer Center API password')
            if errors:
                err_str = ''
                for i, err in enumerate(errors):
                    if i > 0:
                        err_str += '\n'
                    err_str += '- ' + err
                self._show_dialog_critical('Error', 'Parameters missing', 'Some basic parameters are missing.', err_str)
                self._reset_enabled()
            else:
                self.coyodata = CoyoData(cc_baseurl=baseurl,
                                         cc_api_username=username,
                                         cc_api_password=password)
                self._start_cc_processing()
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self._show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), err_str)
            self._reset_enabled()

    def _start_preparing_processing(self):
        """Starts the processing process"""
        logging.debug('Starting preparing processing')

        if not self.loaded_whitelist and self.data.cloudtenants and self.data.cctenants:
            logging.debug('Matching tenants...')
            self.log('Matching tenants...')
            self._callback_processing_finished()
            logging.debug('Done matching tenants.')
            self.log('Done matching tenants.')
        else:
            self._start_preparing_processing_clean()

    def _start_preparing_processing_clean(self):
        """Cleans and starts the processing process"""
        logging.debug('Starting preparing processing (clean)')

        try:
            self._disable()
            self.loaded_whitelist = False
            errors = []
            cloud_baseurl = self.edit_cloud_baseurl.text()
            if not cloud_baseurl:
                logging.error('Invalid cloud base URL')
                errors.append('Please enter a cloud base URL')
            cloud_manage_username = self.edit_cloud_manage_username.text()
            if not cloud_manage_username:
                logging.error('Invalid cloud manage username')
                errors.append('Please enter a cloud manage username')
            cloud_manage_password = self.edit_cloud_manage_password.text()
            if not cloud_manage_password:
                logging.error('Invalid cloud manage password')
                errors.append('Please enter a cloud manage password')
            cc_baseurl = self.edit_cc_baseurl.text()
            if not cc_baseurl:
                logging.error('Invalid Customer Center base URL')
                errors.append('Please enter a Customer Center base URL')
            cc_api_username = self.edit_cc_api_username.text()
            if not cc_api_username:
                logging.error('Invalid Customer Center API username')
                errors.append('Please enter a Customer Center API username')
            cc_api_password = self.edit_cc_api_password.text()
            if not cc_api_password:
                logging.error('Invalid Customer Center API password')
                errors.append('Please enter a Customer Center API password')
            if errors:
                err_str = ''
                for i, err in enumerate(errors):
                    if i > 0:
                        err_str += '\n'
                    err_str += '- ' + err
                self._show_dialog_critical('Error', 'Parameters missing', 'Some basic parameters are missing.', err_str)
                self._reset_enabled()
            else:
                self.coyodata = CoyoData(cloud_baseurl=cloud_baseurl,
                                        cloud_manage_username=cloud_manage_username,
                                        cloud_manage_password=cloud_manage_password,
                                        cc_baseurl=cc_baseurl,
                                        cc_api_username=cc_api_username,
                                        cc_api_password=cc_api_password)
                self._start_processing()
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self._show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), err_str)
            self._reset_enabled()

    def _callback_export_result(self, filename):
        """The export callback, on result

        :param filename: The filename
        """
        logging.debug('Callback: Export result')

        logging.info('Successfully exported to file "{}".'.format(filename))
        self.log('Successfully exported to file "...{}".'.format(filename[-50:]))
        self._enable()

    def _callback_export_error(self, ex):
        """The export callback, on error
        
        :param ex: The exception
        """
        logging.debug('Callback: Export error')

        logging.error('Failed to export to file: "{}"'.format(ex))
        self.log('Failed to export to file.')
        self._show_dialog_critical('Error', 'Error', 'Failed to export to file: "{}"'.format(ex), None)
        self._enable()

    def _callback_export_finished(self):
        """The export callback, on finished"""
        logging.debug('Callback: Export finished')

    def _save_cloud_json(self):
        """Starts the save JSON process"""
        logging.debug('Starting saving cloud tenants to JSON file')

        if self.data.cloudtenants:
            try:
                self._disable()
                options = QFileDialog.Options()
                # options |= QFileDialog.DontUseNativeDialog
                filename, _ = QFileDialog.getSaveFileName(self,
                                                          'Save as JSON file', '', 'JSON Files (*.json);;All Files (*)',
                                                          options=options)
                if filename:
                    logging.debug('Exporting cloud tenants to "{}"...'.format(filename))
                    self.log('Exporting cloud tenants to "{}"...'.format(filename))
                    thread = ExportJSONThread(filename, self.data.cloudtenants)
                    thread.signals.result.connect(self._callback_export_result)
                    thread.signals.error.connect(self._callback_export_error)
                    thread.signals.finished.connect(self._callback_export_finished)
                    self.threadpool.start(thread)
                else:
                    logging.debug('No file name given. Aborting...')
                    self._enable()
            except Exception:
                logging.exception('Could not write JSON to file')
                self.log('Could not write JSON to file.')
                self._enable()

    def _save_cc_json(self):
        """Starts the save JSON process"""
        logging.debug('Starting saving Customer Center tenants to JSON file')

        if self.data.cctenants:
            try:
                self._disable()
                options = QFileDialog.Options()
                # options |= QFileDialog.DontUseNativeDialog
                filename, _ = QFileDialog.getSaveFileName(self,
                                                          'Save as JSON file', '', 'JSON Files (*.json);;All Files (*)',
                                                          options=options)
                if filename:
                    logging.debug('Exporting Customer Center tenants to "{}"...'.format(filename))
                    self.log('Exporting Customer Center tenants to "{}"...'.format(filename))
                    thread = ExportJSONThread(filename, self.data.cctenants)
                    thread.signals.result.connect(self._callback_export_result)
                    thread.signals.error.connect(self._callback_export_error)
                    thread.signals.finished.connect(self._callback_export_finished)
                    self.threadpool.start(thread)
                else:
                    logging.debug('No file name given. Aborting...')
                    self._enable()
            except Exception:
                logging.exception('Could not write JSON to file')
                self.log('Could not write JSON to file.')
                self._enable()

    def _save_json(self):
        """Starts the save JSON process"""
        logging.debug('Starting saving result to JSON file')

        if self.data.difftenants:
            try:
                self._disable()
                options = QFileDialog.Options()
                # options |= QFileDialog.DontUseNativeDialog
                filename, _ = QFileDialog.getSaveFileName(self,
                                                          'Save as JSON file', '', 'JSON Files (*.json);;All Files (*)',
                                                          options=options)
                if filename:
                    logging.debug('Exporting result to "{}"...'.format(filename))
                    self.log('Exporting result to "{}"...'.format(filename))
                    thread = ExportJSONThread(filename, self.data.difftenants)
                    thread.signals.result.connect(self._callback_export_result)
                    thread.signals.error.connect(self._callback_export_error)
                    thread.signals.finished.connect(self._callback_export_finished)
                    self.threadpool.start(thread)
                else:
                    logging.debug('No file name given. Aborting...')
                    self._enable()
            except Exception:
                logging.exception('Could not write JSON to file')
                self.log('Could not write JSON to file.')
                self._enable()
