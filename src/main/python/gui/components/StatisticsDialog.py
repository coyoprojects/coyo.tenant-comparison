#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Statistics dialog"""

import logging
import datetime

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QDialog, QDesktopWidget, QGridLayout, QLabel

class StatisticsDialog(QDialog):
    """Statistics GUI"""

    def __init__(self, data):
        """Initializes the statistics dialog

        :param data: The data
        """
        super().__init__()

        logging.debug('Initializing AboutDialog')

        self.data = data
        self.time = datetime.datetime.now()

        self.setModal(True)

    def init_ui(self):
        """Initiates about dialog UI"""
        self.setWindowTitle('Statistics: COYO Tenant Comparison')

        self.resize(440, 445)
        
        self.font_label = QFont()
        self.font_label.setBold(True)

        self._center()
        self._init_ui()

    def _init_ui(self):
        """Initializes the UI"""
        self.grid = QGridLayout()
        self.grid.setSpacing(10)

        self.label_date = QLabel('Date')
        self.label_date_val = QLabel('{}'.format(self.time))
        self.label_cloudtenants = QLabel('Imported cloud tenants')
        self.label_cloudtenants_val = QLabel('{}'.format(len(self.data.cloudtenants)))
        self.label_cctenants = QLabel('Imported Customer Center tenants')
        self.label_cctenants_val = QLabel('{}'.format(len(self.data.cctenants)))
        self.label_whitelist = QLabel('Imported whitelisted tenants')
        self.label_whitelist_val = QLabel('{}'.format(len(self.data.whitelist)))
        self.label_tenants_difference = QLabel('Difference cloud and Customer Center tenants')
        self.label_tenants_difference_val = QLabel('{}'
            .format(abs(len(self.data.cloudtenants) - len(self.data.cctenants))))
        self.label_difftenants = QLabel('Tenant difference (result)')
        self.label_difftenants_val = QLabel('{}'.format(len(self.data.difftenants)))
        self.label_whitelist_notadded = QLabel('Tenants not added to result due to whitelisting')
        self.label_whitelist_notadded_val = QLabel('{}'.format(len(self.data.difftenants_whitelisted)))
        self.label_whitelist_notmatched = QLabel('Tenants on whitelist not matched')
        self.label_whitelist_notmatched_val = QLabel('{}'
            .format(len(self.data.whitelist) - len(self.data.difftenants_whitelisted)))

        self.label_date.setFont(self.font_label)
        self.label_cloudtenants.setFont(self.font_label)
        self.label_cctenants.setFont(self.font_label)
        self.label_whitelist.setFont(self.font_label)
        self.label_tenants_difference.setFont(self.font_label)
        self.label_difftenants.setFont(self.font_label)
        self.label_whitelist_notadded.setFont(self.font_label)
        self.label_whitelist_notmatched.setFont(self.font_label)

        curr_gridid = 0
        self.grid.addWidget(self.label_date, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_date_val, curr_gridid, 0)

        curr_gridid += 1
        self.grid.addWidget(self.label_cloudtenants, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_cloudtenants_val, curr_gridid, 0)

        curr_gridid += 1
        self.grid.addWidget(self.label_cctenants, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_cctenants_val, curr_gridid, 0)

        curr_gridid += 1
        self.grid.addWidget(self.label_whitelist, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_whitelist_val, curr_gridid, 0)

        curr_gridid += 1
        self.grid.addWidget(self.label_tenants_difference, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_tenants_difference_val, curr_gridid, 0)

        curr_gridid += 1
        self.grid.addWidget(self.label_difftenants, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_difftenants_val, curr_gridid, 0)

        curr_gridid += 1
        self.grid.addWidget(self.label_whitelist_notadded, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_whitelist_notadded_val, curr_gridid, 0)

        curr_gridid += 1
        self.grid.addWidget(self.label_whitelist_notmatched, curr_gridid, 0)
        curr_gridid += 1
        self.grid.addWidget(self.label_whitelist_notmatched_val, curr_gridid, 0)

        self.setLayout(self.grid)

    def _center(self):
        """Centers the window on the screen"""
        screen = QDesktopWidget().screenGeometry()
        self.move((screen.width() - self.geometry().width()) / 2, (screen.height() - self.geometry().height()) / 2)
