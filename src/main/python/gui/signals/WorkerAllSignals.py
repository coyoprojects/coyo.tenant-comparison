#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Worker Signals"""

from PyQt5.QtCore import QObject, pyqtSignal

class WorkerAllSignals(QObject):
    """
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        `object` error data, anything

    result
        `object` data returned from processing, anything
        `object` data returned from processing, anything
    """
    finished = pyqtSignal()
    error = pyqtSignal(object)
    result = pyqtSignal(object, object)
