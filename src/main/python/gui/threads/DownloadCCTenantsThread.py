#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals

class DownloadCCTenantsThread(QRunnable):
    """Download Customer Center tenants thread"""

    def __init__(self, ops):
        """Initializes the thread

        :param ops: The operations
        """
        super().__init__()

        logging.debug('Initializing DownloadCCTenantsThread')

        self.ops = ops

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            logging.debug('Loading Customer Center tenant list...')
            tenantlist = self.ops.get_cc_tenantlist()
        except Exception as ex:
            logging.exception('Failed to download Customer Center tenants: "{}"'.format(ex))
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(tenantlist)
        finally:
            self.signals.finished.emit()
