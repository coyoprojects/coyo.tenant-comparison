#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals

class DownloadCloudTenantsThread(QRunnable):
    """Download cloud tenants thread"""

    def __init__(self, ops):
        """Initializes the thread

        :param ops: The operations
        """
        super().__init__()

        logging.debug('Initializing DownloadCloudTenantsThread')

        self.ops = ops

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            logging.debug('Loading cloud tenant list...')
            tenantlist = self.ops.get_cloud_tenantlist()
        except Exception as ex:
            logging.exception('Failed to download cloud tenants: "{}"'.format(ex))
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(tenantlist)
        finally:
            self.signals.finished.emit()
