#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerAllSignals import WorkerAllSignals

class DownloadTenantsThread(QRunnable):
    """Download thread"""

    def __init__(self, ops):
        """Initializes the thread

        :param ops: The operations
        """
        super().__init__()

        logging.debug('Initializing DownloadTenantsThread')

        self.ops = ops

        self.signals = WorkerAllSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            logging.debug('Loading cloud tenant list...')
            cloudtenants = self.ops.get_cloud_tenantlist()
            logging.debug('Loading Customer Center tenant list...')
            cctenants = self.ops.get_cc_tenantlist()
        except Exception as ex:
            logging.exception('Failed to download all tenants: "{}"'.format(ex))
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(cloudtenants, cctenants)
        finally:
            self.signals.finished.emit()
