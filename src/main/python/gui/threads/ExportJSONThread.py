#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""COYO Tenant Comparison - Export JSON Thread"""

import logging
import os
import json

from PyQt5.QtCore import QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals

class ExportJSONThread(QRunnable):
    """Export to JSON thread"""

    def __init__(self, filename, data):
        """Initializes the thread

        :param filename: The file name
        :param data: The data to export
        """
        super().__init__()

        logging.debug('Initializing ExportJSONThread')

        self.filename = filename
        self.data = data

        self.signals = WorkerSignals()
        if not self.filename.endswith('.json'):
            logging.debug('Adding suffix ".json" to filename "{}"'.format(self.filename))
            self.filename += '.json'

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            logging.info('Exporting data to file "{}"...'.format(self.filename))
            os.makedirs(os.path.dirname(self.filename), exist_ok=True)
            with open(self.filename, 'w', encoding='utf-8') as outfile_json:
                json.dump(self.data, outfile_json, ensure_ascii=False, skipkeys=True)
        except Exception as ex:
            logging.exception('Failed to write JSON file')
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(self.filename)
        finally:
            self.signals.finished.emit()
    