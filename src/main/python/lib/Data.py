#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""Data"""

import logging
import datetime

class Data:
    """Holds the data"""

    def __init__(self):
        self.cloudtenants = []
        self.cctenants = []
        self.whitelist = []
        self.difftenants = []
        self.difftenants_whitelisted = []

    def print_statistics(self):
        now = datetime.datetime.now()
        logging.info('---')
        logging.info('Statistics of {}:'.format(now))
        logging.info('Imported {} COYO Cloud tenants.'.format(len(self.cloudtenants)))
        logging.info('Imported {} COYO Cloud tenants linked to Customer Center.'.format(len(self.cctenants)))
        logging.info('-> Difference: {}'.format(abs(len(self.cloudtenants) - len(self.cctenants))))
        logging.info('Imported {} whitelisted tenants.'.format(len(self.whitelist)))
        logging.info('Could not find {} COYO Cloud tenants in COYO Cloud tenants linked to Customer Center.'
            .format(len(self.difftenants)))
        logging.info('Did not add {} tenants to the final result due to whitelisting.'.format(len(self.difftenants_whitelisted)))
        logging.info('Did not find {} tenants that are whitelisted.'
            .format(len(self.whitelist) - len(self.difftenants_whitelisted)))
        logging.info('---')
