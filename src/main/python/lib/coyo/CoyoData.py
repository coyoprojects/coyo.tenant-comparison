#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""CoyoData - COYO data and credentials"""

class CoyoData:
    """Stores COYO data and and API credentials"""

    def __init__(self,
                 cloud_baseurl='',
                 cloud_manage_username='',
                 cloud_manage_password='',
                 cc_baseurl='',
                 cc_api_username='',
                 cc_api_password=''):
        """Initialization

        :param cloud_baseurl: COYO cloud base URL
        :param cloud_manage_username: COYO cloud management username
        :param cloud_manage_password: COYO cloud management password
        :param cc_baseurl: COYO CC base URL
        :param cc_api_username: COYO CC API username
        :param cc_api_password: COYO CC API password
        """
        self.cloud_baseurl = cloud_baseurl if not cloud_baseurl.endswith('/') else cloud_baseurl[:-1]
        self.cloud_manage_username = cloud_manage_username
        self.cloud_manage_password = cloud_manage_password
        self.cc_baseurl = cc_baseurl if not cc_baseurl.endswith('/') else cc_baseurl[:-1]
        self.cc_api_username = cc_api_username
        self.cc_api_password = cc_api_password
