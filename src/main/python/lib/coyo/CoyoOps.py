#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO Tenant Comparison.
#

"""CoyoOps - COYO Operations"""

import logging

import requests

class CoyoOps:
    """Authenticates with COYO and performs requests"""

    _urls = {
        'cloud.manage.tenants': '/manage/tenants',
        'cc.manage.tenants': '/api/tenants/Coyo4SaasTenant'
    }

    def __init__(self, coyodata):
        """Initialization

        :param coyodata: The CoyoData
        """
        self.coyodata = coyodata

    def get_cloud_tenantlist(self):
        url = self._get_get_cloud_tenantlist_url()
        logging.debug('GET "{}"'.format(url))
        basic_auth = requests.auth.HTTPBasicAuth(self.coyodata.cloud_manage_username, self.coyodata.cloud_manage_password)
        response = requests.get(url, auth=basic_auth)
        return response.json()

    def get_cc_tenantlist(self):
        url = self._get_get_cc_tenantlist_url()
        logging.debug('GET "{}"'.format(url))
        basic_auth = requests.auth.HTTPBasicAuth(self.coyodata.cc_api_username, self.coyodata.cc_api_password)
        response = requests.get(url, auth=basic_auth)
        return response.json()

    def _get_get_cloud_tenantlist_url(self):
        return self.coyodata.cloud_baseurl + self._urls['cloud.manage.tenants']

    def _get_get_cc_tenantlist_url(self):
        return self.coyodata.cc_baseurl + self._urls['cc.manage.tenants']
